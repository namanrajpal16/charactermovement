﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class UIScript : MonoBehaviour {

	public bool shiftDone;
	float translation;
	public Text text;

	// Use this for initialization


	void Start () {
		text = GetComponent<Text> ();
		shiftDone = false;
	}
	
	// Update is called once per frame
	void Update () {
	

		translation = Input.GetAxis ("Vertical") + Input.GetAxis ("Horizontal");
		if (translation > 1) {
				
			if (Input.GetKey (KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) {
				shiftDone = true;
			}


			if (shiftDone == false) {
				text.text = "Great! you can hold down shift to run!";
			} else {
				text.text = "Good!";
				//text.gameObject.GetComponentInParent<GameObject> ().SetActive (false);
			}

		
		}
	}



	public void exit()
	{
		
		Application.Quit ();
	}

}
