﻿using UnityEngine;
using System.Collections;

public class UserControl : MonoBehaviour {

	public float speed = 6f;
	public float rotSpeed = 100f;
	public Animator anim;

	float translation;
	float rotation;
	// Use this for initialization
	void Start () {
	
	}

	void Update()
	{

		translation = Input.GetAxis ("Vertical") * speed;
		rotation = Input.GetAxis ("Horizontal") * rotSpeed;
		translation *= Time.deltaTime;
		rotation *= Time.deltaTime;

		transform.Rotate (0f, rotation, 0f);

		if (translation > 0) {
			//translation *= 0.5f;
			transform.Translate (0f, 0f, translation);

			if (Input.GetKey(KeyCode.LeftShift)) {
				anim.SetBool ("isRunning", true);
				transform.Translate (0f, 0f, translation);
			} else {
				anim.SetBool ("isRunning", false);
				anim.SetBool ("isWalking", true);
			}


		} else {
		
			anim.SetBool ("isWalking", false);
		}





	}



}
